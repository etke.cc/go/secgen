module gitlab.com/etke.cc/go/secgen

go 1.19

require (
	github.com/mikesmitty/edkey v0.0.0-20170222072505-3356ea4e686a
	golang.org/x/crypto v0.17.0
)

require golang.org/x/sys v0.15.0 // indirect
